//
//  NSErrorExtensions.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

extension NSError {

  static var searchErrorDomain = "SearchErrorDomain"

  convenience init(httpStatusCode: Int) {
    let message = HTTPURLResponse.localizedString(forStatusCode: httpStatusCode)
    let userInfo = [NSLocalizedDescriptionKey: message]
    self.init(domain: NSError.searchErrorDomain, code: httpStatusCode, userInfo: userInfo)
  }

  convenience init(message: String) {
    let userInfo = [NSLocalizedDescriptionKey: message]
    self.init(domain: NSError.searchErrorDomain, code: 0, userInfo: userInfo)
  }
}

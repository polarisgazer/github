//
//  UITableViewExtensions.swift
//  GitHub
//
//  Created by [developer name] on 3/23/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit

extension UITableView {

  func registerCell(_ cellClass: AnyClass) {
    let cellClassName = String(describing: cellClass)
    let nib = UINib(nibName: cellClassName, bundle: Bundle(for: cellClass))
    self.register(nib, forCellReuseIdentifier: cellClassName)
  }

}

protocol ViewModelConfigurable {
    func configure(with viewModel: Any)
}

protocol CellDequeable {
    var cellIdentifier: String { get set }
}

protocol CellSelectable {
    var action: ((IndexPath) -> Void) { get set }
}

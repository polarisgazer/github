//
//  UIViewExtensions.swift
//  [project name]
//
//  Created by [developer name] on 3/23/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit

extension UIView {

  class func fromNib<T: UIView>() -> T? {

    let nibName = String(describing: T.self)

    if let nibs = Bundle.main.loadNibNamed(nibName, owner: nil, options: nil),
      let nib = nibs.first as? T {
      return nib
    }

    return nil
  }

}

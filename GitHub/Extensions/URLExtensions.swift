//
//  URLExtensions.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

extension URL {
  func appending(parameters: [String: String]) -> URL? {

    var keyValues: [String] = []

    for (key, value) in parameters {
      keyValues.append("\(key)=\(value)")
    }
    let inlineParameters = keyValues.joined(separator: "&")
    let urlStr = "\(self.absoluteString)?\(inlineParameters)"

    return URL(string: urlStr)
  }
}

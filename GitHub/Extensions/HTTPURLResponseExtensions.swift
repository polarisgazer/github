//
//  HTTPURLResponseExtensions.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

extension HTTPURLResponse {

  var isInformational: Bool {
    return (100...199).contains(self.statusCode)
  }

  var isSuccess: Bool {
    return (200...299).contains(self.statusCode)
  }

  var isRedirect: Bool {
    return (300...399).contains(self.statusCode)
  }

  var isClientError: Bool {
    return (400...499).contains(self.statusCode)
  }

  var isServerError: Bool {
    return (500...599).contains(self.statusCode)
  }
}

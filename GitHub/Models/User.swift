//
//  User.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

struct User: Decodable {
  var login: String
  var avatarURL: String
  var profileURL: String

  enum CodingKeys: String, CodingKey {
    case login
    case avatarURL = "avatar_url"
    case profileURL = "html_url"
  }
}

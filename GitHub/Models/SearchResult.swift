//
//  SearchResult.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

protocol SearchResult {
  var total: Int { get set }
  var isIncomplete: Bool { get set }
}

struct RepositorySearchResult: SearchResult, Decodable {
  var total: Int
  var isIncomplete: Bool
  var repositories: [Repository]

  enum CodingKeys: String, CodingKey {
    case total = "total_count"
    case isIncomplete = "incomplete_results"
    case repositories = "items"
  }

}

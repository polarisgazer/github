//
//  Repository.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

struct Repository: Decodable {
  var name: String
  var owner: User
  var repositoryId: Int
  var numberOfForks: Int
  var subscribersURL: String
  var repositoryDescription: String?

  enum CodingKeys: String, CodingKey {
    case name
    case owner
    case repositoryId = "id"
    case numberOfForks = "forks"
    case subscribersURL = "subscribers_url"
    case repositoryDescription = "description"
  }

}

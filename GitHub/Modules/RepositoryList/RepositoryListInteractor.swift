//
//  RepositoryListInteractor.swift
//  GitHub
//
//  Created by [developer name] on 22/03/2018.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

protocol RepositoryListInteractorProtocol {
  func getRepositories(by keyword: String, completion: @escaping ([Repository], Error?) -> Void)
  func repository(at index: Int) -> Repository?
}

class RepositoryListInteractor: RepositoryListInteractorProtocol {

  var searchService: SearchServiceProtocol
  var cachedRepositories: [Repository] = []

  init(searchService: SearchServiceProtocol) {
    self.searchService = searchService
  }

  func getRepositories(by keyword: String, completion: @escaping ([Repository], Error?) -> Void) {

    searchService.fetchRepositories(by: keyword) { [weak self] (repositories, error) in
      self?.cachedRepositories = repositories
      completion(repositories, error)
    }
  }

  func repository(at index: Int) -> Repository? {
    return self.cachedRepositories[index]
  }

}

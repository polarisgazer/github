//
//  RepositoryListView.swift
//  GitHub
//
//  Created by [developer name] on 22/03/2018.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit

protocol RepositoryListViewProtocol: class {
  func display(_ viewModel: RepositoryListViewModel)
  func startLoading()
  func stopLoading()
  func dismissSearchController()
}

struct RepositoryListViewModel {
  var title: String
  var infoMessage: String = ""
  var cellViewModels: [RepositoryCellViewModel]
}

class RepositoryListViewController: UIViewController, RepositoryListViewProtocol, UISearchBarDelegate {

  var presenter: RepositoryListPresenterProtocol!
  var searchController: UISearchController?
  var dataSource: RepositoryListDataSource?

  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var infoLabel: UILabel!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

  override func viewDidLoad() {
    super.viewDidLoad()
    configureTableView()
    configureSearchBar()
    presenter.present()
  }

  func display(_ viewModel: RepositoryListViewModel) {

    navigationItem.title = viewModel.title
    infoLabel.text = viewModel.infoMessage

    dataSource = RepositoryListDataSource()
    dataSource?.source = viewModel.cellViewModels
    tableView.dataSource = dataSource
    tableView.delegate = dataSource

    tableView.reloadData()
  }

  func startLoading() {
    infoLabel.text = ""
    activityIndicator.startAnimating()
  }

  func stopLoading() {
    activityIndicator.stopAnimating()
  }

  func dismissSearchController() {
    searchController?.isActive = false
  }

  func configureTableView() {
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 44
    tableView.registerCell(RepositoryCell.self)
  }

  func configureSearchBar() {
    searchController = UISearchController(searchResultsController: nil)
    searchController?.dimsBackgroundDuringPresentation = false
    searchController?.searchBar.delegate = self
    tableView.tableHeaderView = searchController?.searchBar
  }

  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    if let keyword = searchBar.text {
      presenter.presentRepositories(by: keyword)
    }
  }

}

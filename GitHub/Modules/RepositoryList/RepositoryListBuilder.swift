//
//  RepositoryListBuilder.swift
//  GitHub
//
//  Created by [developer name] on 22/03/2018.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit
import Swinject

protocol RepositoryListBuilderProtocol {
  func getViewController() -> RepositoryListViewController!
}

class RepositoryListBuilder: RepositoryListBuilderProtocol {

  let container = Container(parent: ServicesContainer.default)

  func getViewController() -> RepositoryListViewController! {

    container.register(RepositoryListInteractorProtocol.self) { resolver in
      let searchService = resolver.resolve(SearchServiceProtocol.self)!
      let interactor = RepositoryListInteractor(searchService: searchService)
      return interactor
    }

    container.register(RepositoryListViewController.self) { _ in
      RepositoryListViewController(nibName: String(describing: RepositoryListViewController.self),
                                   bundle: Bundle(for: RepositoryListViewController.self))
      }.initCompleted { resolver, service in
        service.presenter = resolver.resolve(RepositoryListPresenterProtocol.self)
    }

    container.register(RepositoryListRouterProtocol.self) { resolver in
      let router = RepositoryListRouter()
      router.viewController = resolver.resolve(RepositoryListViewController.self)
      return router
    }

    container.register(RepositoryListPresenterProtocol.self) { resolver in
      let presenter = RepositoryListPresenter()
      presenter.view = resolver.resolve(RepositoryListViewController.self)
      presenter.interactor = resolver.resolve(RepositoryListInteractorProtocol.self)
      presenter.router = resolver.resolve(RepositoryListRouterProtocol.self)
      return presenter
    }

    return container.resolve(RepositoryListViewController.self)!
  }

  deinit {
    container.removeAll()
  }

}

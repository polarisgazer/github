//
//  RepositoryCellViewModel.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit

struct RepositoryCellViewModel: CellDequeable, CellSelectable {

  var cellIdentifier: String = "RepositoryCell"
  var action: ((IndexPath) -> Void)

  var repoName: String
  var repoDescription: String
  var numberOfForks: String
  var avatarURL: URL?
  var avatarPlaceholder: UIImage?

  init(repoName: String,
       repoDescription: String,
       numberOfForks: String,
       avatarURL: URL?,
       avatarPlaceholder: UIImage?,
       action: @escaping ((IndexPath) -> Void)) {

    self.repoName = repoName
    self.repoDescription = repoDescription
    self.numberOfForks = numberOfForks
    self.avatarURL = avatarURL
    self.avatarPlaceholder = avatarPlaceholder
    self.action = action
  }

}

//
//  RepositoryListDataSource.swift
//  GitHub
//
//  Created by [developer name] on 3/23/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit

class RepositoryListDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {

  var source: [CellDequeable] = []

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return source.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    let viewModel = source[indexPath.row]
    let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.cellIdentifier, for: indexPath)

    if let configurableCell = cell as? ViewModelConfigurable {
      configurableCell.configure(with: viewModel)
    }
    return cell
  }

  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 0.01
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)

    let viewModel = source[indexPath.row]

    if let selectableCell = viewModel as? CellSelectable {
      selectableCell.action(indexPath)
    }
  }

}

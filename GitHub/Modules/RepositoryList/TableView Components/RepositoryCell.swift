//
//  RepositoryCell.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryCell: UITableViewCell, ViewModelConfigurable {

  @IBOutlet weak var repoName: UILabel!
  @IBOutlet weak var ownerAvatar: UIImageView!
  @IBOutlet weak var numberOfForks: UILabel!
  @IBOutlet weak var repoDescription: UILabel!

  func configure(with viewModel: Any) {
    if let viewModel = viewModel as? RepositoryCellViewModel {

      ownerAvatar.sd_setImage(with: viewModel.avatarURL,
                              placeholderImage: viewModel.avatarPlaceholder,
                              options: .cacheMemoryOnly,
                              completed: nil)

      repoName.text = viewModel.repoName
      numberOfForks.text = viewModel.numberOfForks
      repoDescription.text = viewModel.repoDescription
    }
  }

}

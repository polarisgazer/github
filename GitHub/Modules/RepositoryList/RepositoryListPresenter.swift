//
//  RepositoryListPresenter.swift
//  GitHub
//
//  Created by [developer name] on 22/03/2018.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit

protocol RepositoryListPresenterProtocol {
  func present()
  func presentRepositories(by keyword: String)

}

class RepositoryListPresenter: RepositoryListPresenterProtocol {

  weak var view: RepositoryListViewProtocol!
  var interactor: RepositoryListInteractorProtocol!
  var router: RepositoryListRouterProtocol!

  func present() {
    let viewModel = firstAppearanceViewModel()
    view?.display(viewModel)
  }

  func presentRepositories(by keyword: String) {
    view.startLoading()

    interactor.getRepositories(by: keyword) { [weak self] (repositories, error) in

      DispatchQueue.main.async {
        self?.view.stopLoading()

        if let serviceError = error {
          self?.present(error: serviceError)
          return
        }

        if let viewModel = self?.repositoryListViewModel(from: repositories) {
          self?.view?.display(viewModel)
        }

      }
    }

  }

  func present(repository: Repository?) {
    if let repository = repository {
      view.dismissSearchController()
      router.routeToRepositoryDetail(with: repository)
    }
  }

  private func present(error: Error) {
    let viewModel = errorViewModel(from: error)
    view.display(viewModel)
  }

  // MARK: ViewModels

  func cellViewModel(from repository: Repository) -> RepositoryCellViewModel {

    let action: ((IndexPath) -> Void) = { [weak self] indexPath in
      let repository = self?.interactor.repository(at: indexPath.row)
      self?.present(repository: repository)
    }

    return RepositoryCellViewModel(repoName: repository.name,
                                   repoDescription: repository.repositoryDescription ?? "No description",
                                   numberOfForks: "\(repository.numberOfForks) forks",
                                   avatarURL: URL(string: repository.owner.avatarURL),
                                   avatarPlaceholder: UIImage(named: "no_avatar"),
                                   action: action)
  }

  func firstAppearanceViewModel() -> RepositoryListViewModel {
    return RepositoryListViewModel(title: "GitHub",
                                   infoMessage: "Search for GitHub repositories",
                                   cellViewModels: [])
  }

  func errorViewModel(from error: Error) -> RepositoryListViewModel {
    return RepositoryListViewModel(title: "GitHub",
                                   infoMessage: error.localizedDescription,
                                   cellViewModels: [])
  }

  func repositoryListViewModel(from repositories: [Repository]) -> RepositoryListViewModel {

    let cellViewModels = repositories.map { repo in
      return cellViewModel(from: repo)
    }
    let message = cellViewModels.isEmpty ? "No items matching your search criteria were found" : ""

    return RepositoryListViewModel(title: "GitHub",
                            infoMessage: message,
                            cellViewModels: cellViewModels)
  }

}

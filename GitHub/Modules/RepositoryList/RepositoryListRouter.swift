//
//  RepositoryListRouter.swift
//  GitHub
//
//  Created by [developer name] on 22/03/2018.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit

protocol RepositoryListRouterProtocol {
  func routeToRepositoryDetail(with repository: Repository)
}

class RepositoryListRouter: RepositoryListRouterProtocol {

  weak var viewController: UIViewController?

  func routeToRepositoryDetail(with repository: Repository) {
    let controller = RepositoryBuilder().getViewController(with: repository)
    viewController?.navigationController?.pushViewController(controller, animated: true)
  }

}

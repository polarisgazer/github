//
//  RepositoryBuilder.swift
//  GitHub
//
//  Created by [developer name] on 24/03/2018.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit

protocol RepositoryBuilderProtocol {
  func getViewController(with repository: Repository) -> RepositoryViewController
}

class RepositoryBuilder: RepositoryBuilderProtocol {

  func getViewController(with repository: Repository) -> RepositoryViewController {

    let viewController = RepositoryViewController(nibName: String(describing: RepositoryViewController.self),
                                                  bundle: Bundle(for: RepositoryViewController.self))
    let interactor = RepositoryInteractor(repository: repository,
                                          repositoryService: ServicesContainer.repository)
    let router = RepositoryRouter()
    router.viewController = viewController

    let presenter = RepositoryPresenter()
    presenter.view = viewController
    presenter.interactor = interactor
    presenter.view = viewController

    viewController.presenter = presenter

    return viewController
  }
}

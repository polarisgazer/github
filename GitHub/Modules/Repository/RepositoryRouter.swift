//
//  RepositoryRouter.swift
//  GitHub
//
//  Created by [developer name] on 24/03/2018.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit

protocol RepositoryRouterProtocol {

}

class RepositoryRouter: RepositoryRouterProtocol {

  weak var viewController: UIViewController?

}

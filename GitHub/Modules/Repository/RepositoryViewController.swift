//
//  RepositoryView.swift
//  GitHub
//
//  Created by [developer name] on 24/03/2018.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit
import SDWebImage

protocol RepositoryViewProtocol: class {
  func display(_ viewModel: RepositoryViewModel)
  func startLoading()
  func stopLoading()
}

struct RepositoryViewModel {
  var title: String
  var infoMessage: String = ""
  var headerViewModel: RepositoryHeaderViewModel
  var subscribersViewModels: [UserCellViewModel]
}

class RepositoryViewController: UIViewController, RepositoryViewProtocol {

  var presenter: RepositoryPresenterProtocol!
  var dataSource: UsersDataSource?

  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var infoLabel: UILabel!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

  override func viewDidLoad() {
    super.viewDidLoad()

    configureTableView()
    presenter.present()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    presenter.presentSubscribers()
  }

  func display(_ viewModel: RepositoryViewModel) {

    navigationItem.title = viewModel.title
    infoLabel.text = viewModel.infoMessage

    dataSource = UsersDataSource()
    dataSource?.source = viewModel.subscribersViewModels
    tableView.dataSource = dataSource
    tableView.delegate = dataSource

    if let header: RepositoryHeaderView = UIView.fromNib() {
      header.configure(with: viewModel.headerViewModel)
      tableView.tableHeaderView = header
    }

    tableView.reloadData()
  }

  func startLoading() {
    infoLabel.text = ""
    activityIndicator.startAnimating()
  }

  func stopLoading() {
    activityIndicator.stopAnimating()
  }

  func configureTableView() {
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 44
    tableView.registerCell(UserCell.self)
  }

}

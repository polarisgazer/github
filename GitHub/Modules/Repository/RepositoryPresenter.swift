//
//  RepositoryPresenter.swift
//  GitHub
//
//  Created by [developer name] on 24/03/2018.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit

protocol RepositoryPresenterProtocol {
  func present()
  func presentSubscribers()
}

class RepositoryPresenter: RepositoryPresenterProtocol {

  weak var view: RepositoryViewProtocol!
  var interactor: RepositoryInteractorProtocol!
  var router: RepositoryRouterProtocol!

  func present() {
    let repository = interactor.getRepository()
    let viewModel = repositoryViewModel(from: repository, isFirstAppearance: true)
    view?.display(viewModel)
  }

  func presentSubscribers() {
    view.startLoading()

    interactor.getSubscribers { [weak self] (subscribers: [User], error: Error?) in
      DispatchQueue.main.async {
        self?.view.stopLoading()

        if let serviceError = error,
          let repository = self?.interactor.getRepository() {
          self?.present(error: serviceError, for: repository)
          return
        }

        if let repository = self?.interactor.getRepository(),
          let viewModel = self?.repositoryViewModel(from: repository, subscribers: subscribers) {
          self?.view?.display(viewModel)
        }

      }
    }
  }

  private func present(error: Error, for repository: Repository) {
    let viewModel = errorViewModel(from: error, repository: repository)
    view.display(viewModel)
  }
}

extension RepositoryPresenter {
  // MARK: ViewModels

  func repositoryViewModel(from repository: Repository,
                           isFirstAppearance: Bool = false) -> RepositoryViewModel {

    let header = headerViewModel(from: repository,
                                 numberOfSubscribers: 0,
                                 shouldShowNumberOfSubscribers: !isFirstAppearance)

    return RepositoryViewModel(title: "Repository",
                               infoMessage: "Fetching subscribers...",
                               headerViewModel: header,
                               subscribersViewModels: [])
  }

  func headerViewModel(from repository: Repository,
                       numberOfSubscribers: Int,
                       shouldShowNumberOfSubscribers: Bool = true) -> RepositoryHeaderViewModel {

    var subscribers = ""
    if shouldShowNumberOfSubscribers {
      subscribers = numberOfSubscribers > 0 ? "\(numberOfSubscribers) subscribers" : "No subscribers"
    }

    return RepositoryHeaderViewModel(repoName: repository.name,
                                     ownerAvatarURL: URL(string: repository.owner.avatarURL),
                                     avatarPlaceholder: UIImage(named: "no_avatar"),
                                     numberOfSubscribers: subscribers)
  }

  func userCellViewModel(from user: User) -> UserCellViewModel {
    return UserCellViewModel(login: user.login,
                             avatarURL: URL(string: user.avatarURL),
                             avatarPlaceholder: UIImage(named: "no_avatar"))
  }

  func repositoryViewModel(from repository: Repository, subscribers: [User]) -> RepositoryViewModel {

    let cellViewModels = subscribers.map { user in
      return userCellViewModel(from: user)
    }

    let header = headerViewModel(from: repository, numberOfSubscribers: subscribers.count)

    return RepositoryViewModel(title: "Repository",
                               infoMessage: "",
                               headerViewModel: header,
                               subscribersViewModels: cellViewModels)
  }

  func errorViewModel(from error: Error, repository: Repository) -> RepositoryViewModel {

    let header = headerViewModel(from: repository, numberOfSubscribers: 0)

    return RepositoryViewModel(title: "Repository",
                               infoMessage: error.localizedDescription,
                               headerViewModel: header,
                               subscribersViewModels: [])
  }
}

//
//  RepositoryInteractor.swift
//  GitHub
//
//  Created by [developer name] on 24/03/2018.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

protocol RepositoryInteractorProtocol {
  func getSubscribers(completion: @escaping ([User], Error?) -> Void)
  func getRepository() -> Repository
}

class RepositoryInteractor: RepositoryInteractorProtocol {

  var repositoryService: RepositoryServiceProtocol
  var repository: Repository
  var cachedSubscribers: [User]?

  init(repository: Repository, repositoryService: RepositoryServiceProtocol) {
    self.repository = repository
    self.repositoryService = repositoryService
  }

  func getRepository() -> Repository {
    return repository
  }

  func getSubscribers(completion: @escaping ([User], Error?) -> Void) {

    if let subscribers = cachedSubscribers {
      completion(subscribers, nil)
    }

    repositoryService.fetchSubscribers(for: repository.owner.login,
                                       repo: repository.name,
                                       completion: { [weak self] (subscribers, error) in

                                        self?.cachedSubscribers = subscribers
                                        completion(subscribers, error)
    })
  }

}

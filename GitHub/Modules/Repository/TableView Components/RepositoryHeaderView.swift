//
//  RepositoryHeaderView.swift
//  GitHub
//
//  Created by [developer name] on 3/23/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit
import SDWebImage

struct RepositoryHeaderViewModel {
  var repoName: String
  var ownerAvatarURL: URL?
  var avatarPlaceholder: UIImage?
  var numberOfSubscribers: String
}

class RepositoryHeaderView: UIView, ViewModelConfigurable {

  @IBOutlet weak var avatar: UIImageView!
  @IBOutlet weak var repoName: UILabel!
  @IBOutlet weak var numberOfSubscribers: UILabel!

  func configure(with cellViewModel: Any) {

    if let viewModel = cellViewModel as? RepositoryHeaderViewModel {

      avatar.sd_setImage(with: viewModel.ownerAvatarURL,
                         placeholderImage: viewModel.avatarPlaceholder,
                         options: .cacheMemoryOnly,
                         completed: nil)

      repoName.text = viewModel.repoName
      numberOfSubscribers.text = viewModel.numberOfSubscribers
    }

  }

}

//
//  UserCell.swift
//  GitHub
//
//  Created by [developer name] on 3/23/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit
import SDWebImage

class UserCell: UITableViewCell, ViewModelConfigurable {

  @IBOutlet weak var login: UILabel!
  @IBOutlet weak var avatar: UIImageView!

  func configure(with viewModel: Any) {
    if let viewModel = viewModel as? UserCellViewModel {

      avatar.sd_setImage(with: viewModel.avatarURL,
                              placeholderImage: viewModel.avatarPlaceholder,
                              options: .cacheMemoryOnly,
                              completed: nil)

      login.text = viewModel.login
    }
  }

}

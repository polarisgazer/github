//
//  RepositoryCellViewModel.swift
//  GitHub
//
//  Created by [developer name] on 3/23/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import UIKit

struct UserCellViewModel: CellDequeable {

  var cellIdentifier: String = "UserCell"

  var login: String
  var avatarURL: URL?
  var avatarPlaceholder: UIImage?

  init(login: String,
       avatarURL: URL?,
       avatarPlaceholder: UIImage?) {

    self.login = login
    self.avatarURL = avatarURL
    self.avatarPlaceholder = avatarPlaceholder
  }

}

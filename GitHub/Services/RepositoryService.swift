//
//  RepositoryService.swift
//  GitHub
//
//  Created by [developer name] on 3/23/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

struct RepositoryServiceURLComponents {

  var subscribers: String

  init(owner: String, repo: String) {
    self.subscribers = String(format: "/repos/%@/%@/subscribers", owner, repo)
  }
}

protocol RepositoryServiceProtocol {
  func fetchSubscribers(for owner: String, repo: String, completion: @escaping ([User], Error?) -> Void)
}

class RepositoryService: RepositoryServiceProtocol {

  var networkService: NetworkServiceProtocol
  var baseURL: URL

  init(networkService: NetworkServiceProtocol, baseURL: URL) {
    self.networkService = networkService
    self.baseURL = baseURL
  }

  func fetchSubscribers(for owner: String, repo: String, completion: @escaping ([User], Error?) -> Void) {

    let urlComponents = RepositoryServiceURLComponents(owner: owner, repo: repo)
    let url = URL(string: urlComponents.subscribers, relativeTo: baseURL)
    if let validURL = url {

      networkService.get(url: validURL, parameters: [:]) { (data: Data?, error: Error?) in

        if error != nil {
          completion([], error)
          return
        }

        if let validData = data {
          do {
            let users = try JSONDecoder().decode([User].self, from: validData)
            completion(users, nil)
          } catch {
            completion([], error)
          }
          return
        }

        completion([], NSError(message: "DetailsFetcherService: no valid data received"))
      }
      return
    }

  }

}

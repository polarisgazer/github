//
//  ServicesContainer.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation
import Swinject

class ServicesContainer {

  static var `default`: Container = {
    let container = Container()

    registerServices(in: container)

    return container
  }()

  private static func registerServices(in container: Container) {

    container.register(NetworkServiceProtocol.self) { _ in
      let configuration = URLSessionConfiguration.default
      let urlSession = URLSession(configuration: configuration)
      return NetworkService(urlSession: urlSession)
    }

    container.register(SearchServiceProtocol.self) { resolver in
      let networkService = resolver.resolve(NetworkServiceProtocol.self)!
      let baseURL = URL(string: Configuration.Endpoints.base)!
      return SearchService(networkService: networkService, baseURL: baseURL)
    }

    container.register(RepositoryServiceProtocol.self) { resolver in
      let networkService = resolver.resolve(NetworkServiceProtocol.self)!
      let baseURL = URL(string: Configuration.Endpoints.base)!
      return RepositoryService(networkService: networkService, baseURL: baseURL)
    }

  }
// For demo only
    static var network: NetworkServiceProtocol {
        let configuration = URLSessionConfiguration.default
        let urlSession = URLSession(configuration: configuration)
        return NetworkService(urlSession: urlSession)
    }

    static var search: SearchServiceProtocol {
        let networkService = ServicesContainer.network
        let baseURL = URL(string: Configuration.Endpoints.base)!
        return SearchService(networkService: networkService, baseURL: baseURL)
    }

    static var repository: RepositoryServiceProtocol {
        let networkService = ServicesContainer.network
        let baseURL = URL(string: Configuration.Endpoints.base)!
        return RepositoryService(networkService: networkService, baseURL: baseURL)
    }
}

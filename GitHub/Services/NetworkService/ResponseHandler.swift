//
//  ResponseHandler.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

class ResponseHandler {

  static func handle(response: URLResponse?,
                     data: Data?,
                     error: Error?,
                     completion: (Data?, Error?) -> Void) {

    if error != nil {
      completion(nil, error)
      return
    }

    if let validResponse = response as? HTTPURLResponse {

      if validResponse.isSuccess {
        completion(data, nil)
        return
      }

      let serviceError = NSError(httpStatusCode: validResponse.statusCode)
      completion(nil, serviceError)
      return
    }

    if let validData = data,
      let textData = String(data: validData, encoding: .utf8) {
      completion(nil, NSError(message: textData))
      return
    }

    completion(nil, nil)
  }
}

//
//  NetworkService.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

protocol NetworkServiceProtocol {
  func get(url: URL, parameters: [String: String], completion:@escaping (Data?, Error?) -> Void)
}

class NetworkService: NetworkServiceProtocol {

  var urlSession: URLSession

  init(urlSession: URLSession) {
    self.urlSession = urlSession
  }

  internal func get(url: URL,
                    parameters: [String: String],
                    completion:@escaping (Data?, Error?) -> Void) {

    if let serviceURL = url.appending(parameters: parameters) {

      var request = URLRequest(url: serviceURL)
      request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      request.setValue("application/vnd.github.v3+json", forHTTPHeaderField: "Accept")
      request.httpMethod = "GET"

      let task = urlSession.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in

        ResponseHandler.handle(response: response,
                               data: data,
                               error: error,
                               completion: completion)
      }

      task.resume()
    }
  }

}

//
//  SearchService.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

struct SearchServiceURLComponents {
  static var repositories = "search/repositories"
}

struct SearchServiceURLParameters {
  static var query = "q"
}

protocol SearchServiceProtocol {
  func fetchRepositories(by keyword: String, completion: @escaping ([Repository], Error?) -> Void)
}

class SearchService: SearchServiceProtocol {

  private var networkService: NetworkServiceProtocol
  private var baseURL: URL

  init(networkService: NetworkServiceProtocol, baseURL: URL) {
    self.networkService = networkService
    self.baseURL = baseURL
  }

  func fetchRepositories(by keyword: String, completion: @escaping ([Repository], Error?) -> Void) {
    let url = URL(string: SearchServiceURLComponents.repositories, relativeTo: baseURL)!
    let parameters = [SearchServiceURLParameters.query: keyword]

    networkService.get(url: url, parameters: parameters) { (data: Data?, error: Error?) in

      if error != nil {
        completion([], error)
        return
      }

      if let validData = data {
        do {
          let searchResult = try JSONDecoder().decode(RepositorySearchResult.self, from: validData)
            completion(searchResult.repositories, nil)
        } catch {
          completion([], error)
        }
        return
      }

      completion([], NSError(message: "SearchService: no valid data received"))
    }

  }
}

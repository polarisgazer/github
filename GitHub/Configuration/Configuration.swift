//
//  Configuration.swift
//  GitHub
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Foundation

struct Configuration {

  struct Endpoints {
    static var base = "https://api.github.com/"
  }

}

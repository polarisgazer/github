//
//  ServicesContainerSpec.swift
//  GitHubTests
//
//  Created by [developer name] on 3/22/18.
//  Copyright © 2018 [company name]. All rights reserved.
//

import Quick
import Nimble
import Swinject

@testable import GitHub

class ServicesContainerSpec: QuickSpec {

  override func spec() {

    let container = Container(parent: ServicesContainer.default)

    describe("ServicesContainer") {
      context("When default services have been loaded") {

        it("NetworkService should not be nil") {
          let networkService = container.resolve(NetworkService.self)
          expect(networkService).toNot(beNil())
        }

        it("SearchService should not be nil") {
          let searchService = container.resolve(SearchService.self)
          expect(searchService).toNot(beNil())
        }

        it("RepositoryService should not be nil") {
          let searchService = container.resolve(RepositoryService.self)
          expect(searchService).toNot(beNil())
        }

      }
    }

  }

}

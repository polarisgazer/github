# GitHub

[![Swift Version][swift-image]][swift-url] [![CocoaPods Compatible](https://img.shields.io/cocoapods/v/EZSwiftExtensions.svg)](https://img.shields.io/cocoapods/v/LFAlertController.svg) [![Platform](https://img.shields.io/cocoapods/p/LFAlertController.svg?style=flat)](http://cocoapods.org/pods/LFAlertController)

Fetches a list of GitHub repositories and displays their subscribers.


## Features

- Displays the list of available GitHub repositpries based on a search by repo's name.
- Display the subscribers of the selected repository.

## Requirements

- iOS 9.0+
- Xcode 9.2

## Tools and Dependencies

### Ruby

```
Ruby v2.5.0
```

### Tools

```
Bundler,   ~>1.16  // Gem version manager
Homebrew,  ~>1.3.0 // Package Manager
Cocoapods, ~>1.4.0 // Dependency Manager Framework
Generamba, ~>1.4.0 // VIPER modules generator
SwiftLint, ~>0.23  // Enforces Swift style and conventions
```

### Libraries

```
Swinject,   ~>2.1.0 // Dependency Injection Framework
SDWebImage, ~>4.2.0 // Asynchronous image downloader & cacher
```
## Setup

Make sure your ruby version is 2.5.0

- Install bundler:

```gem install bundler```

- Install gems:

```bundle install```

- Install cocoapods libraries:

```bundle exec pod install```

##Run
At this point the app should be able to be run on iOS 9.0+ simulator.

##Generamba

Automate [VIPER](https://www.objc.io/issues/13-architecture/viper/) modules' generation with ```generamba```:

- Install gem:

```gem install generamba```

- Install templates:

```generamba template install```

This will install [viper template](https://bitbucket.org/polarisgazer/viper) (it's public and free) in ```Templates``` folder.

Feel free to remove the template from version control:

```
rm -rf /Templates/viper/.git
```

Start using generamba by adding and completing the Rambafile. Being in the project's root folder, simply run and follow the instructions:

```
generamba setup
```

Example: add the ```Login``` module:

```generamba gen Login viper```

(where "Login" is the module name, and "viper" is the name of the selected VIPER template)

Accordingly to the paths specified in [Rambafile](https://github.com/rambler-digital-solutions/Generamba/wiki/Rambafile-Structure), this will add the following files:

```
GitHub/Modules/LoginView.swift
GitHub/Modules/LoginView.xib
GitHub/Modules/LoginPresenter.swift
GitHub/Modules/LoginInteractor.swift
GitHub/Modules/LoginRouter.swift
GitHub/Modules/LoginBuilder.swift
```

For more information please see the README in:

```
Templates/viper/README.md
```

##SwiftLint

To enforce Swift style and conventions, install SwiftLint:

```brew install swiftlint```

Use ```swiftlint``` and ```swiftlint autocorrect``` to check and autocorrect code style violations.

## Meta

Developer – [developer name] – polarisgazer@gmail.com


[swift-image]:https://img.shields.io/badge/swift-4.0-orange.svg
[swift-url]: https://swift.org/
